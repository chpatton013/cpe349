package edu.calpoly.csc349.FixedJobs;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
import java.util.Comparator;
import java.util.List;
import java.util.LinkedList;

import edu.calpoly.csc349.FixedJobs.Scheduler;
import edu.calpoly.csc349.FixedJobs.Scheduler.Job;
import edu.calpoly.csc349.FixedJobs.Scheduler.JobTime;
import edu.calpoly.csc349.FixedJobs.GreedyFixedJobs;
import edu.calpoly.csc349.FixedJobs.GreedyFixedJobs.MyJob;

public class ScheduleTester {
   static int minTime = 0, // precondition 3
    maxTime = 20000,
    minPeople = 1, // preconditions 2, 3
    maxPeople = 20000,
    minValue = 0, // precondition 3
    maxValue = 200,
    minJobs = 0, // precondition 3
    maxJobs = 500000;
   static long seed;
   static Random rn;
   static GreedyFixedJobs gfj = new GreedyFixedJobs();
   static Comparator<JobTime> jtComp = new Comparator<JobTime>() {
      public int compare(JobTime jt1, JobTime jt2) {
         return jt1.time - jt2.time;
      }
   };

   private static boolean isSorted(JobTime[] arr) {
      for (int ndx = 1; ndx < arr.length; ++ndx)
         if (jtComp.compare(arr[ndx - 1], arr[ndx]) > 1)
            return false;
      return true;
   }

   private static void initTimes(int[] tArr) {
      int timeSlots = 0;

      for (int ndx = 0; ndx < tArr.length; ++ndx) {
         tArr[ndx] = (maxTime != minTime ? Math.abs(rn.nextInt()) %
          (maxTime - minTime) : 0) + minTime;
         timeSlots += tArr[ndx];
      }

      // precondition 2
      maxJobs = maxJobs < timeSlots ? maxJobs : timeSlots;
   }

   private static void initJobs(Job[] jArr) {
      int dl, val;

      for (int ndx = 0; ndx < jArr.length; ++ndx) {
         dl = (maxTime != minTime ? Math.abs(rn.nextInt()) %
          (maxTime - minTime) : 0) + minTime;
         val = (maxValue != minValue ? Math.abs(rn.nextInt()) %
          (maxValue - minValue) : 0) + minValue;
         // precondition 4
         jArr[ndx] = new MyJob(dl, val);
      }
   }

   private static int calcScore(JobTime[] jtArr) {
      int score = 0;
      for (int ndx = 0; ndx < jtArr.length; ++ndx)
         if (jtArr != null && jtArr[ndx].job.getDeadline() > jtArr[ndx].time)
            score += jtArr[ndx].job.getValue();
      return score;
   }

   private static int test1() {
      // precondition 2
      int[] tArr = new int[minPeople];
      // precondition 1
      Job[] jArr = new Job[minJobs];

      initTimes(tArr);
      initJobs(jArr);

      return testConditions(tArr, jArr);
   }

   private static int test2() {
      int[] tArr = new int[maxPeople];
      Job[] jArr = new Job[minJobs];

      initTimes(tArr);
      initJobs(jArr);

      return testConditions(tArr, jArr);
   }

   private static int test3() {
      int[] tArr = new int[minPeople];
      Job[] jArr = new Job[maxJobs];

      initTimes(tArr);
      initJobs(jArr);

      return testConditions(tArr, jArr);
   }

   private static int test4() {
      int[] tArr = new int[maxPeople];
      Job[] jArr = new Job[maxJobs];

      initTimes(tArr);
      initJobs(jArr);

      return testConditions(tArr, jArr);
   }

   private static int test5() {
      int[] tArr = new int[Math.abs(rn.nextInt()) % (maxPeople - minPeople) +
       minPeople];
      Job[] jArr = new Job[Math.abs(rn.nextInt()) % (maxJobs - minJobs) +
       minJobs];

      initTimes(tArr);
      initJobs(jArr);

      return testConditions(tArr, jArr);
   }

   public static int testConditions(int[] tArr, Job[] jArr) {
      int[] tClone = tArr.clone(), countArr = new int[jArr.length];
      Job[] jClone = jArr.clone();
      JobTime[] jtArr = gfj.makeSchedule(tArr, jArr);
      List<Job> jList = new LinkedList<Job>(Arrays.asList(jArr));
      boolean flag = true;
      int ndx1, ndx2, count = 0;

      //postcondition 1: sorted
      if (!isSorted(jtArr))
         return -1;
     
      //postcondition 2: exactly one instance
      for (ndx1 = 0; ndx1 < jArr.length; ++ndx1, count = 0)
         for (ndx2 = 0; ndx2 < jArr.length; ++ndx2)
            if (jArr[ndx1] == jtArr[ndx2].job && ++count > 1)
               return -2;

      //postcondition 3: jobs not altered
      if (!Arrays.equals(jArr, jClone))
         return -3;

      //postcondition 4: wTimes not altered
      if (!Arrays.equals(tArr, tClone))
         return -4;

      //postcondition 5: no new jobs
      for (ndx1 = 0; ndx1 < jArr.length; ++ndx1)
         for (ndx2 = 0; ndx2 < jArr.length; ++ndx2)
            if (jArr[ndx1] == jtArr[ndx2].job)
               ++countArr[ndx1];
      for (ndx1 = 0; ndx1 < countArr.length; ++ndx1)
         if (countArr[ndx1] != 1)
            return -5;

      return calcScore(jtArr);
   }

   public static void main(String[] args) {
      if (args.length != 0) {
         if(args[0].equals("-r")) {
            rn = new Random();
            System.out.printf("random: %d", test5());
         }
         else {
            Scanner sc = new Scanner(System.in);
            seed = sc.nextLong();
            rn = new Random(seed);
            System.out.printf("random (seed:%d): %d", seed, test5());
         }
      }
      else {
         rn = new Random(seed);
         System.out.println("min,min: " + test1());
         System.out.println("max,min: " + test2());
         System.out.println("min,max: " + test3());
         System.out.println("max,max: " + test4());
      }
   }
}
