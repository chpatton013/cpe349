import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class ClosestPairLab {
   static final public double kRange = 1000.0;
   static private int cmpCount;
   
   static public class Point {
      public double x;
      public double y;
      public int xRank;
      
      public double distance(Point pt2) {
         return Math.sqrt((pt2.x - x)*(pt2.x - x) + (pt2.y - y)*(pt2.y - y));
      }
   }
   
   static public class ClosePair {
      public Point p1;
      public Point p2;
      public double distance;

      public ClosePair(Point p1, Point p2) {
         this.p1 = p1;
         this.p2 = p2;
         this.distance = p1.distance(p2);
      }
   }

   public static void main(String[] args) {
      int ndx;
      Scanner in = new Scanner(System.in);
      Point[] xAxis, yAxis;
      ClosePair res;
      
      xAxis = rndPts(in.nextInt(), in.nextInt());
      
      yAxis = xAxis.clone();
      Arrays.sort(xAxis, new Comparator<Point> () {
         public int compare(Point p1, Point p2) {
            return p1.x < p2.x ? -1 : (p1.x == p2.x ? 0 : 1);
         }
      });
      for (ndx = 0; ndx < xAxis.length; ndx++)
         xAxis[ndx].xRank = ndx;
      
      Arrays.sort(yAxis, new Comparator<Point> () {
         public int compare(Point p1, Point p2) {
            return p1.y < p2.y ? -1 : (p1.y == p2.y ? 0 : 1);
         }
      });
      
      res = findClosest(xAxis, yAxis, 0, xAxis.length);
      System.out.printf("After %d compares, (%f, %f) and {%f, %f) are closest"
       + " at %f\n", cmpCount, res.p1.x, res.p1.y, res.p2.x, res.p2.y,
       res.distance);
   }

   // Create and return a Point[] of size "dim", with random Point elements,
   // set up using "seed" as the random seed.
   static public Point[] rndPts(int seed, int dim) {
      Point[] rtn = new Point[dim];
      Random rnd = new Random(seed);

      for (int ndx = 0; ndx < dim; ndx++) {
         rtn[ndx] = new Point();
         rtn[ndx].x = kRange * rnd.nextDouble();
         rtn[ndx].y = kRange * rnd.nextDouble();
      }
      return rtn;
   }

   // Take a set of points, sorted both by x in xAxis and y in yAxis (both 
   // arrays contain the same objects, in different orders.  Deal only with
   // those points whose indexes in xAxis are in [lo,hi).  Return the closest
   // pair among those points if there are at least two points; null otherwise.
   static ClosePair findClosest(Point[] xAxis, Point[] yAxis, int lo, int hi) {
      int numPts, firstRight, leftNdx, rightNdx, ndx, aboveNdx;
      Point[] leftYAxis, rightYAxis, chnYAxis;
      ClosePair rtn = null, left, right;
      Point chnPt;
      List<Point> chnList;
      double delta, bound;
      
      numPts = hi-lo;
      if (numPts < 2)           // Only happens if we start with 0 or 1 points.
         return null;
      else if (numPts < 4) {    // Handle base case of 2 or 3 points
         rtn = new ClosePair(xAxis[lo], xAxis[lo+1]);
         if (numPts == 3) {
            if (xAxis[lo+1].distance(xAxis[lo+2]) < rtn.distance)
               rtn = new ClosePair(xAxis[lo+1], xAxis[lo+2]);


            // entry area 1

            if (xAxis[lo].distance(xAxis[lo+2]) < rtn.distance)
               rtn = new ClosePair(xAxis[lo], xAxis[lo+2]);

            // end entry area 1


         }
      }
      else {
         // Determine first index  to go on the right side
         firstRight = lo + (numPts/2);
         
         // Set up filtered Y axis arrays for left and right sides.
         leftYAxis = new Point[firstRight - lo];
         rightYAxis = new Point[hi - firstRight];
         leftNdx = rightNdx = 0;
         for (Point pt: yAxis) {
            if (pt.xRank < firstRight)
               leftYAxis[leftNdx++] = pt;
            else
               rightYAxis[rightNdx++] = pt;
         }
         assert leftNdx == leftYAxis.length && rightNdx == rightYAxis.length;
         
         // Recursively call for left and right subproblems.
         left = findClosest(xAxis, leftYAxis, lo, firstRight);
         right = findClosest(xAxis, rightYAxis, firstRight, hi);
         rtn = left.distance < right.distance ? left : right;
         
         // Pick minimal distance as "delta", and find boundary x-value
         delta = rtn.distance;


         // entry area 2

         bound = (xAxis[firstRight].x + xAxis[firstRight - 1].x) / 2;

         // end entry area 2


         // Filter out channel points, in ascending Y order


         // entry area 3

         chnYAxis = new Point[0];
         chnList = new LinkedList<Point>();
         for (Point p : yAxis) {
            if (p.x < bound && p.x + delta > bound || p.x > bound && p.x - delta < bound)
               chnList.add(p);
         }
         chnYAxis = chnList.toArray(chnYAxis);

         // end entry area 3


         // Traverse channel, seeking pairs closer than delta
         for (ndx = 0; ndx < chnYAxis.length; ndx++) {
            chnPt = chnYAxis[ndx];
            
            // Examine at most "delta" above our current point.
            for (aboveNdx = ndx+1; aboveNdx < chnYAxis.length
             && chnYAxis[aboveNdx].y < chnPt.y+delta; aboveNdx++) {
               cmpCount++;
               if (chnPt.distance(chnYAxis[aboveNdx]) < rtn.distance)
                  rtn = new ClosePair(chnYAxis[aboveNdx], chnPt);
            }
         }
      }
      
      return rtn;
   }
}
