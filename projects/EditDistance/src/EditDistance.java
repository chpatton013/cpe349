package edu.calpoly.csc349;

import java.util.Set;
import java.util.EnumSet;
import java.util.Arrays;
import java.util.List;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import java.util.Comparator;

import java.util.Scanner;

public class EditDistance {
   private enum Edit {
      Copy {
         protected int cost() {
            return COPY_COST;
         }
         protected boolean canApply(SubSln ss) {
            return ss.row > 0 && ss.col > 0 &&
             ss.tbl.s1.charAt(ss.row - 1) == ss.tbl.s2.charAt(ss.col - 1);
         }
         protected SubSln parentOf(SubSln ss) {
            return ss.tbl.arr[ss.row - 1][ss.col - 1];
         }
         protected String editString(SubSln ss) {
            return "Copy '" + ss.tbl.s1.charAt(ss.row - 1) + "' unchanged";
         }
      },
      Ins {
         protected int cost() {
            return INS_COST;
         }
         protected boolean canApply(SubSln ss) {
            return ss.col > mode - 1;
         }
         protected SubSln parentOf(SubSln ss) {
            return ss.tbl.arr[ss.row][ss.col - mode];
         }
         protected String editString(SubSln ss) {
            return "Insert '" + ss.tbl.s2.substring(ss.col - mode, ss.col) +
             "'";
         }
      },
      Ins1 {
         protected int cost() {
            return Ins.cost();
         }
         protected boolean canApply(SubSln ss) {
            mode = MODE_ONE;
            return Ins.canApply(ss);
         }
         protected SubSln parentOf(SubSln ss) {
            mode = MODE_ONE;
            return Ins.parentOf(ss);
         }
         protected String editString(SubSln ss) {
            mode = MODE_ONE;
            return Ins.editString(ss);
         }
      },
      Ins2 {
         protected int cost() {
            return Ins.cost();
         }
         protected boolean canApply(SubSln ss) {
            mode = MODE_TWO;
            return Ins.canApply(ss);
         }
         protected SubSln parentOf(SubSln ss) {
            mode = MODE_TWO;
            return Ins.parentOf(ss);
         }
         protected String editString(SubSln ss) {
            mode = MODE_TWO;
            return Ins.editString(ss);
         }
      },
      Ins3 {
         protected int cost() {
            return Ins.cost();
         }
         protected boolean canApply(SubSln ss) {
            mode = MODE_THREE;
            return Ins.canApply(ss);
         }
         protected SubSln parentOf(SubSln ss) {
            mode = MODE_THREE;
            return Ins.parentOf(ss);
         }
         protected String editString(SubSln ss) {
            mode = MODE_THREE;
            return Ins.editString(ss);
         }
      },
      Del {
         protected int cost() {
            return DEL_COST;
         }
         protected boolean canApply(SubSln ss) {
            return ss.row > mode - 1;
         }
         protected SubSln parentOf(SubSln ss) {
            return ss.tbl.arr[ss.row - mode][ss.col];
         }
         protected String editString(SubSln ss) {
            return "Delete " + mode + " chars";
         }
      },
      Del1 {
         protected int cost() {
            return Del.cost();
         }
         protected boolean canApply(SubSln ss) {
            mode = MODE_ONE;
            return Del.canApply(ss);
         }
         protected SubSln parentOf(SubSln ss) {
            mode = MODE_ONE;
            return Del.parentOf(ss);
         }
         protected String editString(SubSln ss) {
            mode = MODE_ONE;
            return Del.editString(ss);
         }
      },
      Del2 {
         protected int cost() {
            return Del.cost();
         }
         protected boolean canApply(SubSln ss) {
            mode = MODE_TWO;
            return Del.canApply(ss);
         }
         protected SubSln parentOf(SubSln ss) {
            mode = MODE_TWO;
            return Del.parentOf(ss);
         }
         protected String editString(SubSln ss) {
            mode = MODE_TWO;
            return Del.editString(ss);
         }
      },
      Del3 {
         protected int cost() {
            return Del.cost();
         }
         protected boolean canApply(SubSln ss) {
            mode = MODE_THREE;
            return Del.canApply(ss);
         }
         protected SubSln parentOf(SubSln ss) {
            mode = MODE_THREE;
            return Del.parentOf(ss);
         }
         protected String editString(SubSln ss) {
            mode = MODE_THREE;
            return Del.editString(ss);
         }
      },
      Repl {
         protected int cost() {
            return REPL_COST;
         }
         protected boolean canApply(SubSln ss) {
            return ss.row > 0 && ss.col > 0 &&
             ss.tbl.s1.charAt(ss.row - 1) != ss.tbl.s2.charAt(ss.col - 1);
         }
         protected SubSln parentOf(SubSln ss) {
            return ss.tbl.arr[ss.row - 1][ss.col - 1];
         }
         protected String editString(SubSln ss) {
            return "Replace '" + ss.tbl.s1.charAt(ss.row - 1) + "' with '"
             + ss.tbl.s2.charAt(ss.col - 1) + "'";
         }
      },
      Prm {
         protected int cost() {
            return mode;
         }
         protected boolean canApply(SubSln ss) {
            List<Character> chList1, chList2;

            if (ss.row < mode || ss.col < mode)
               return false;

             chList1 = new LinkedList<Character>(Arrays.asList(
              toCharacterArray(ss.tbl.s1.substring(
              ss.row - mode, ss.row))));

             chList2 = new LinkedList<Character>(Arrays.asList(
              toCharacterArray(ss.tbl.s2.substring(
              ss.col - mode, ss.col))));

            for (Character c : chList1)
               if (!chList2.remove(c))
                  return false;

            return true;
         }
         protected SubSln parentOf(SubSln ss) {
            return ss.tbl.arr[ss.row - mode][ss.col - mode];
         }
         protected String editString(SubSln ss) {
            return "Swap '" + ss.tbl.s1.substring(ss.row - mode, ss.row) +
             "' to '" + ss.tbl.s2.substring(ss.col - mode, ss.col)+ "'";
         }
      },
      Prm2 {
         protected int cost() {
            mode = MODE_TWO;
            return Prm.cost();
         }
         protected boolean canApply(SubSln ss) {
            mode = MODE_TWO;
            return Prm.canApply(ss);
         }
         protected SubSln parentOf(SubSln ss) {
            mode = MODE_TWO;
            return Prm.parentOf(ss);
         }
         protected String editString(SubSln ss) {
            mode = MODE_TWO;
            return Prm.editString(ss);
         }
      },
      Prm3 {
         protected int cost() {
            mode = MODE_THREE;
            return Prm.cost();
         }
         protected boolean canApply(SubSln ss) {
            mode = MODE_THREE;
            return Prm.canApply(ss);
         }
         protected SubSln parentOf(SubSln ss) {
            mode = MODE_THREE;
            return Prm.parentOf(ss);
         }
         protected String editString(SubSln ss) {
            mode = MODE_THREE;
            return Prm.editString(ss);
         }
      },
      ChBl {
         protected int cost() {
            return CHBL_COST;
         }
         protected boolean canApply(SubSln ss) {
            int bl1, bl2;

            if (ss.row == 0 || ss.col == 0 ||
             !Character.isWhitespace(ss.tbl.s1.charAt(ss.row - 1)) ||
             !Character.isWhitespace(ss.tbl.s2.charAt(ss.col - 1)))
               return false;

            bl1 = numSpacesBefore(ss.tbl.s1, ss.row - 1);
            bl2 = numSpacesBefore(ss.tbl.s2, ss.col - 1);

            return bl1 > 0 || bl2 > 0;
         }
         protected SubSln parentOf(SubSln ss) {
            int bl1, bl2, ndx1, ndx2, minNdx1, minNdx2, bound1, bound2;

            bl1 = numSpacesBefore(ss.tbl.s1, ss.row - 1);
            bl2 = numSpacesBefore(ss.tbl.s2, ss.col - 1);

            if (false && bl1 != bl2) {
               return ss.tbl.arr[ss.row - bl1 - 1][ss.col - bl2 - 1];
            }
            else {
               bound1 = ss.row - bl1 - 1;
               bound2 = ss.col - bl2 - 1;

               if (bl1 == bl2) {
                  minNdx1 = ss.row - 1;
                  minNdx2 = ss.col - 2;
               }
               else {
                  minNdx1 = bound1;
                  minNdx2 = bound2;
               }

               for (ndx1 = ss.row - 1; ndx1 >= bound1; --ndx1) {
                  for (ndx2 = ss.col - 1; ndx2 >= bound2; --ndx2) {
                     if (ss.tbl.arr[ndx1][ndx2].cost <
                      ss.tbl.arr[minNdx1][minNdx2].cost &&
                      (ss.row - ndx1) != (ss.col - ndx2)) {
                        minNdx1 = ndx1;
                        minNdx2 = ndx2;
                     }
                  }
               }

               return ss.tbl.arr[minNdx1][minNdx2];
            }
         }
         protected String editString(SubSln ss) {
            SubSln p = ChBl.parentOf(ss);
            return "Convert " + (ss.row - p.row) + " blanks to " +
             (ss.col - p.col) + " blanks";
         }
      },
      ChCs {
         protected int cost() {
            return CHCS_COST;
         }
         protected boolean canApply(SubSln ss) {
            return ss.row > 0 && ss.col > 0 &&
             ss.tbl.s1.charAt(ss.row - 1) != ss.tbl.s2.charAt(ss.col - 1) &&
             Character.toLowerCase(ss.tbl.s1.charAt(ss.row - 1)) ==
             Character.toLowerCase(ss.tbl.s2.charAt(ss.col - 1));
         }
         protected SubSln parentOf(SubSln ss) {
            return ss.tbl.arr[ss.row - 1][ss.col - 1];
         }
         protected String editString(SubSln ss) {
            return "Convert case from '" +
             ss.tbl.s1.charAt(ss.row - 1) + "' to '" +
             ss.tbl.s2.charAt(ss.col - 1) + "'";
         }
      };

      final int COPY_COST = 1;
      final int INS_COST = 6;
      final int DEL_COST = 3;
      final int REPL_COST = 7;
      final int CHBL_COST = 1;
      final int CHCS_COST = 1;

      final int MODE_ONE = 1;
      final int MODE_TWO = 2;
      final int MODE_THREE = 3;

      static int mode = 0;
      static Edit[] editArrRef = new Edit[0];;
      static List<Edit> publicEdits = new LinkedList<Edit> (Arrays.asList(
       EnumSet.complementOf(EnumSet.of(Ins, Del, Prm)).toArray(editArrRef)));

      protected abstract int cost();
      protected abstract boolean canApply(SubSln ss);
      protected abstract SubSln parentOf(SubSln ss);
      protected abstract String editString(SubSln ss);
   }

   private class SubSln {
      int row, col, cost;
      SlnTbl tbl;
      List<Edit> applicableEdits, minCostEdits;

      public SubSln(SlnTbl tbl, int row, int col) {
         this.row = row;
         this.col = col;
         this.tbl = tbl;

         cost = 0;

         applicableEdits = new LinkedList<Edit>();
         minCostEdits = new LinkedList<Edit>();
      }

      public String toString() {
         String rtn = "";

         rtn += "(" + row + "," + col + ") - ";
         rtn += "applicableEdits:" + applicableEdits + ", ";
         rtn += "minCostEdits:" + minCostEdits;

         return rtn;
      }

      protected void solve() {
         int minCost, currCost;

         for (Edit e : Edit.publicEdits)
            if (e.canApply(this))
               applicableEdits.add(e);

         if (applicableEdits.size() > 0) {
            minCost = applicableEdits.get(0).cost() +
             applicableEdits.get(0).parentOf(this).cost;
            for (Edit e : applicableEdits) {
               currCost = e.cost() + e.parentOf(this).cost;
               if (currCost < minCost) {
                  minCost = currCost;
                  minCostEdits.clear();
               }
               if (currCost == minCost) {
                  minCostEdits.add(e);
               }
            }

            cost = minCost;
         }

         for (Edit e : minCostEdits)
            this.tbl.editTally.put(e, this.tbl.editTally.get(e) + 1);
      }

      protected String editListToString() {
         int ndx;
         String rtn = "";

         for (ndx = 0; ndx < minCostEdits.size(); ++ndx) {
            rtn += minCostEdits.get(ndx);
            if (ndx != minCostEdits.size() - 1)
               rtn += ",";
         }

         return rtn;
      }
   }

   private class SlnTbl {
      List<Edit> edits = Edit.publicEdits;

      int rows, cols, maxNumEdits = 0;
      String s1, s2;
      SubSln[][] arr;
      Map<Edit, Integer> editTally = new TreeMap<Edit, Integer>(
         new Comparator<Edit>() {
            public int compare(Edit e1, Edit e2) {
               return e1.toString().compareTo(e2.toString());
            }
         }
      );

      public SlnTbl(String s1, String s2) {
         int ndx1, ndx2;

         rows = s1.length() + 1;
         cols = s2.length() + 1;

         this.s1 = s1;
         this.s2 = s2;

         arr = new SubSln[rows][cols];

         for (Edit e : Edit.publicEdits)
            editTally.put(e, new Integer(0));

         for (ndx1 = 0; ndx1 < rows; ++ndx1)
            for (ndx2 = 0; ndx2 < cols; ++ndx2)
               arr[ndx1][ndx2] = new SubSln(this, ndx1, ndx2);

         for (ndx1 = 0; ndx1 < rows; ++ndx1) {
            for (ndx2 = 0; ndx2 < cols; ++ndx2) {
               arr[ndx1][ndx2].solve();
               if (arr[ndx1][ndx2].minCostEdits.size() > maxNumEdits)
                  maxNumEdits = arr[ndx1][ndx2].minCostEdits.size();
            }
         }
      }

      void printCost() {
         System.out.printf("Best cost is: %d\n\n", arr[rows - 1][cols - 1].cost);
      }

      void printEdits() {
         for (Edit e : Edit.publicEdits)
            System.out.printf("%s used %d times.\n", e, editTally.get(e));
         System.out.printf("\n");
      }

      void printOptimalSln() {
         SubSln ss;
         Deque<String> stk = new LinkedList<String>();

         System.out.printf("Edits are:\n");
         for (ss = arr[rows - 1][cols - 1]; ss.row + ss.col != 0;
          ss = ss.minCostEdits.get(0).parentOf(ss))
            stk.push(ss.minCostEdits.get(0).editString(ss));

         while (!stk.isEmpty())
            System.out.printf("%s\n", stk.poll());
         System.out.printf("\n");
      }

      void printTable() {
         int cellWidth, ndx1, ndx2;
         char[] chArr1 = s1.toCharArray(), chArr2 = s2.toCharArray();

         cellWidth = maxNumEdits == 0 ? DFLT_CELL_WIDTH :
          maxNumEdits * DFLT_CELL_WIDTH;

         System.out.printf("%-" + (4 + cellWidth) + "s ", "");
         for (char ch : chArr2)
            System.out.printf("%-" + cellWidth + "s ", "(" + ch + ")");
         System.out.printf("\n\n");

         System.out.printf("    ");
         for (SubSln ss : arr[0])
            System.out.printf("%-" + cellWidth + "d ", ss.cost);
         System.out.printf("\n");

         System.out.printf("    ");
         for (SubSln ss : arr[0])
            System.out.printf("%-" + cellWidth + "s ", ss.editListToString());
         System.out.printf("\n\n");

         for (ndx1 = 1; ndx1 < rows; ++ndx1) {
            System.out.printf("(%c) ", chArr1[ndx1 - 1]);
            for (ndx2 = 0; ndx2 < cols; ++ndx2)
               System.out.printf("%-" + cellWidth + "d ", arr[ndx1][ndx2].cost);
            System.out.printf("\n");

            System.out.printf("    ");
            for (ndx2 = 0; ndx2 < cols; ++ndx2)
               System.out.printf("%-" + cellWidth + "s ",
                arr[ndx1][ndx2].editListToString());
            System.out.printf("\n\n");
         }
      }

      void printSln() {
         printCost();
         printEdits();
         printOptimalSln();
         printTable();
      }
   }

   final int DFLT_CELL_WIDTH = 5;

   public EditDistance(String s1, String s2) {
      SlnTbl slnTbl = new SlnTbl(s1, s2);
      slnTbl.printSln();
   }

   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      EditDistance app = new EditDistance(sc.nextLine(), sc.nextLine());
      sc.close();
   }

   private static int numSpacesBefore(String s, int pos) {
      int ndx = pos - 1;

      while (ndx >= 0 && Character.isWhitespace(s.charAt(ndx)))
         --ndx;

      return pos - ndx - 1;
   }

   private static Character[] toCharacterArray(String s) {
      int ndx;
      Character[] arr = new Character[s.length()];

      for (ndx = 0; ndx < s.length(); ++ndx)
         arr[ndx] = s.charAt(ndx);

      return arr;
   }
}
