package edu.calpoly.csc349.FixedJobs;

public interface Scheduler {
   
   // A job to be performed
   public interface Job {
      public int getDeadline();  // Get nonnegative deadline for this job
      public int getValue();     // Get nonnegative value for this job
   }
   
   public class JobTime {
      public Job job;            // Job to perform
      public int time;           // Time at which to schedule it

      public JobTime(Job job, int time) {
         this.job = job;
         this.time = time;
      }
   }
   
   // Given stopping times for 1 or more workers, and a set of
   // jobs for which to schedule the workers, arrange a schedule describing
   // at what time to schedule each job, such that the total value of jobs
   // that are completed by their deadline is maximized.  
   // 
   // Preconditions:  
   // 1. "jobs" is not guaranteed to be sorted, and it
   // may even have 0 elements, but it will not be null.
   // 2. "wTimes" will have at least one element, and the total of its elements
   // will at least equal jobs.length.
   // 3. All stopping times, values and deadlines are nonnegative.
   // 4. All elements of "jobs" are nonnull.
   //
   // Postconditions: 
   // 1. The returned array must be sorted by increasing starting time.
   // 2. Every Job must appear exactly once in the returned array.
   // 3. The jobs array passed in must not be altered.
   // 4. The wTimes array passed in must not be altered.
   // 5. Only Jobs from the "jobs" array may appear in the returned array.
   // 6. Algorithm must run in near-linear time in the number of jobs and times.
   // 7. Returned array complies with requirements for fixed jobs problem and
   //    provides an optimum solution.
   public JobTime[] makeSchedule(int[] wTimes, Job[] jobs); 
}