package edu.calpoly.csc349.Knapsack;

import java.util.Collections;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class BBKnapsack {
   public class LinkStack<T> implements Cloneable {
      protected class Node {
         Object data;
         Node next;

         public Node(Object d, Node n) {data = d; next = n;}
      };

      protected Node mHead;
      int mSize;

      public LinkStack() {mHead = null; mSize = 0;}

      public void push(T val) {mHead = new Node(val, mHead); ++mSize;}
      public void pop() {mHead = mHead.next; --mSize;}
      public T top() {return (T)mHead.data;}

      public boolean isEmpty() {return mSize == 0;}
      public int size() {return mSize;}

      public LinkStack<T> clone() {
         LinkStack<T> copy = null;
         Node temp, end = null;

         try {copy = (LinkStack)super.clone();}
         catch (CloneNotSupportedException err) {}

         return (LinkStack<T>)copy;
      }

      public String toString() {
         int ndx;
         String rtn = "";
         Object[] rev = new Object[mSize];
         Node nd = mHead;

         for (ndx = mSize-1; ndx >= 0; --ndx, nd = nd.next)
            rev[ndx] = nd.data;

         for (ndx = 0; ndx < mSize; ++ndx)
            rtn += rev[ndx].toString() + "  ";

         return rtn;
      }
   }

   private static class Item {
      int weight, value, index;

      public Item(int w, int v) {weight = w; value = v;}

      public String toString() {return "w " + weight + " v " + value;}
   }

   private class Rtn {
      int steps = 0, cuts = 0, bestWeight = 0, bestValue = 0, slns = 0;
      LinkStack<Item> slnItems = new LinkStack<Item>();

      public String toString() {
         String rtn;

         rtn = String.format("\nAnswer after %d steps, %d cuts, %d new slns: ",
          steps, cuts, slns);

         rtn += slnItems.toString();

         rtn += String.format("\nTotal weight: %d  Total value: %d\n",
          bestWeight, bestValue);

         return rtn;
      }
   }

   private int bestGreedyValue(int ndx, int weight) {
      double value = 0.0;

      for (; ndx < itemArr.length && weight + itemArr[ndx].weight < maxWeight;
       ++ndx) {
         weight += itemArr[ndx].weight;
         value += itemArr[ndx].value;
      }

      if (ndx < itemArr.length && weight < maxWeight) {
         value += itemArr[ndx].value /
          (itemArr[ndx].weight / (double)(maxWeight - weight));
      }

      return (int)value;
   }

   private void setup() {
      int ndx;

      rtn = new Rtn();

      Arrays.sort(itemArr, new Comparator<Item>() {
         public int compare(Item i1, Item i2) {
            double d1 = i1.value / (double)i1.weight,
             d2 = i2.value / (double)i2.weight;

            return d1 < d2 ? 1 : -1;
         }
      });

      for (ndx = 0; ndx < itemArr.length; ++ndx)
         itemArr[ndx].index = ndx;
   }

   public void solve() {
      boolean chooseFlag;
      int baseNdx, midNdx, topNdx, currWeight, currValue, bestGreedyValue;
      Item thisItem;
      LinkStack<Item> stk = new LinkStack<Item>();

      setup();

      for (baseNdx = 0; baseNdx < itemArr.length; ++baseNdx) {
         thisItem = itemArr[baseNdx];
         currWeight = currValue = 0;
         chooseFlag = false;

         if (bestGreedyValue(baseNdx, currWeight) <= rtn.bestValue) {
            ++rtn.cuts;
            break;
         }
         else {
            midNdx = baseNdx;
            do {
               for (topNdx = midNdx; topNdx < itemArr.length; ++topNdx) {
                  thisItem = itemArr[topNdx];

                  if (currValue + bestGreedyValue(topNdx, currWeight) <=
                   rtn.bestValue) {
                     ++rtn.cuts;
                     break;
                  }
                  else {
                     ++rtn.steps;

                     if (currWeight + thisItem.weight <= maxWeight) {
                        if (chooseFlag) chooseFlag = false;
                        else {
                           stk.push(thisItem);
                           currValue += thisItem.value;
                           currWeight += thisItem.weight;
                        }
                     }
                  }
               }

               if (topNdx == itemArr.length && currValue > rtn.bestValue) {
                  ++rtn.slns;
                  rtn.bestValue = currValue;
                  rtn.bestWeight = currWeight;

                  rtn.slnItems = stk.clone();
               }

               thisItem = stk.top();
               stk.pop();

               midNdx = thisItem.index + 1;
               currValue -= thisItem.value;
               currWeight -= thisItem.weight;
            } while (!stk.isEmpty());
         }
      }
   }


   static int maxWeight;
   static Item[] itemArr;
   static Rtn rtn;

   public BBKnapsack() {
      int ndx;
      Scanner sc = new Scanner(System.in);

      System.out.printf("Enter total weight, num items, and item wgt/val: ");
      maxWeight = sc.nextInt();
      itemArr = new Item[sc.nextInt()];

      for (ndx = 0; ndx < itemArr.length; ++ndx)
         itemArr[ndx] = new Item(sc.nextInt(), sc.nextInt());

      solve();
      System.out.printf("%s\n", rtn);
   }

   public static void main(String[] args) {
      BBKnapsack app = new BBKnapsack();
   }
}
