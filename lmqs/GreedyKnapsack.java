import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class GreedyKnapsack {
   // One knapsack item
   static private class Item {
      public int weight;  // Weight of the item
      public int value;   // Value of the item

      public Item(int weight, int value) {
         this.weight = weight;
         this.value = value;
      }
   }
   // Answer with total value in |value|, and items used, in descending
   // value-density order, in |items|.  Last item may be too large to
   // fit; truncation of last item is understood.
   static public class Answer {
      public double value;
      public Item[] items;  // In descending value-density order
      
      public Answer(double value, Item[] items) {
         this.value = value;
         this.items = items;
      }
   }

   // Prompt for total weight, number of items, and item data.  Set up
   // array of items, and call findBestPack, printing back out the value and
   // weight of each item selected.
   public static void main(String[] args) {
      Scanner in = new Scanner(System.in);
      Item[] items;
      int weight;
      Answer ans;
      
      System.out.print("Enter allowed weight, num items, and item w/v: ");
      weight = in.nextInt();
      items = new Item[in.nextInt()];
      for (int x = 0; x < items.length; x++)
         items[x] = new Item(in.nextInt(), in.nextInt());
      
      ans = findBestPack(weight, items);
      System.out.printf("Best pack has value %.2f\n", ans.value);
      for (Item item : ans.items)
         System.out.printf("w %d v %d\n", item.weight, item.value);
   }
   
   public static Answer findBestPack(int weight, Item[] items) {


      int ndx, tw = 0;
      double tv = 0.0;

      Arrays.sort(items, new Comparator<Item>() {
         public int compare(Item i1, Item i2) {
            double d1 = i1.value / (double)i1.weight,
             d2 = i2.value / (double)i2.weight;
            return d1 <= d2 ? 1 : -1;
         }
      });

      for (ndx = 0; ndx < items.length &&
       tw + items[ndx].weight < weight; ++ndx) {
         tw += items[ndx].weight;
         tv += items[ndx].value;
      }

      if (ndx < items.length && tw < weight) {
         tv += items[ndx].value / ((double)(items[ndx].weight /
          (double)(weight - tw)));
         ++ndx;
      }
      else if (tw != weight && ndx != 0) {
         --ndx;
      }
      items = Arrays.copyOfRange(items, 0, ndx);

      return new Answer(tv, items);



   }
}
