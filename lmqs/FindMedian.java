import java.util.*;

public class FindMedian {
   public static Random rnd = new Random();
   
   public static void main(String args[]) {
      int ndx;
      double vals[];
      Scanner input = new Scanner(System.in);

      vals = new double[input.nextInt()];
      for (ndx = 0; ndx < vals.length; ndx++)
         vals[ndx] = input.nextDouble();

      System.out.printf("Median is: %.3f\n", findMedian(vals));
   }

   public static int prt(double[] vals, int lo, int hi) {
      int endOfLow, toPlace, rndNdx;
      double pivot, temp;
      
      rndNdx = lo + rnd.nextInt(hi - lo);
      temp = vals[hi-1];
      vals[hi-1] = vals[rndNdx];
      vals[rndNdx] = temp;

      endOfLow = lo;                // No vals on the bottom yet
      pivot = vals[hi-1];           // Use last value as pivot

      for (toPlace = lo; toPlace < hi-1; toPlace++)
         if (vals[toPlace] < pivot) {       // vals[toPlace] goes in bottom part
            temp = vals[toPlace];
            vals[toPlace] = vals[endOfLow];
            vals[endOfLow] = temp;
            endOfLow++;
         }                         
         // No else needed -- vals[toPlace] stays in top part by default

      vals[hi-1] = vals[endOfLow];  // Move pivot to middle
      vals[endOfLow] = pivot;
      
      return endOfLow;                    // Return pivot location
   }
   
   public static double findMedian(double[] vals) {
      int md, lo = 0, hi = vals.length;

      //

      do {
         md = prt(vals, lo, hi);

         if (md == lo)
            ++lo;
         else if (md < vals.length / 2)
            lo = md;
         else
            hi = md;
      } while(hi > lo && md != vals.length / 2);

      return vals[md];

      //

   }
}
