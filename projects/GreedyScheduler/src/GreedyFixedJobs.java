package edu.calpoly.csc349.FixedJobs;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;

import edu.calpoly.csc349.FixedJobs.Scheduler;

public class GreedyFixedJobs implements Scheduler {
   public static class MyJob implements Job {
      int deadline, value;

      public MyJob(int deadline, int value) {
         this.deadline = deadline > 0 ? deadline : 0;
         this.value = value > 0 ? value : 0;
      }

      public int getDeadline() {return deadline;}
      public int getValue() {return value;}

      public String toString() {
         return "(" + deadline + "," + value + ")";
      }

      public boolean equals(Object other) {
         MyJob o;

         if (other == null || !(other instanceof MyJob))
            return false;

         o = (MyJob)other;
         return value == o.value && deadline == o.deadline;
      }
   }

   private class TimeNode {
      int time, quantity, parentIndex;

      public TimeNode(int time, int quantity) {
         this.time = time;
         this.quantity = quantity;
      }

      public String toString() {
         return "(t:" + time + ", q:" + quantity + ", p:" + parentIndex + ")";
      }
   }

   private class TimeNodeSet {
      int ndx;
      TimeNode[] tnArr;
      Deque<TimeNode> stk = new LinkedList<TimeNode>();

      public TimeNodeSet(TimeNode[] arr) {
         tnArr = arr;
         for (ndx = 0; ndx < tnArr.length; ++ndx)
            tnArr[ndx].parentIndex = ndx - 1;
      }

      private int getLatestTime() {
         for (ndx = tnArr.length - 1; ndx >= 0; ndx = tnArr[ndx].parentIndex) {
            if (tnArr[ndx].quantity > 0) {
               --tnArr[ndx].quantity;
               break;
            }
            else
               stk.push(tnArr[ndx]);
         }

         for (; !stk.isEmpty(); stk.peek().parentIndex = ndx, stk.pop())
            ;

         return ndx;
      }

      private int getLatestTimeBeforeDeadline(Job j) {
         for (ndx = Math.min(j.getDeadline() - 1, tnArr.length - 1); ndx >= 0;
          ndx = tnArr[ndx].parentIndex) {
            if (tnArr[ndx].quantity > 0) {
               --tnArr[ndx].quantity;
               break;
            }
            else
               stk.push(tnArr[ndx]);
         }

         for (; !stk.isEmpty(); stk.peek().parentIndex = ndx, stk.pop())
            ;

         return ndx;
      }

      public String toString() {
         return Arrays.deepToString(tnArr);
      }
   }

   private static Comparator<Job> jComp = new Comparator<Job>() {
      public int compare(Job j1, Job j2) {
         int val = j2.getValue() - j1.getValue();
         return val == 0 ? j1.getDeadline() - j2.getDeadline() : val;
      }
   };

   private static Comparator<JobTime> jtComp = new Comparator<JobTime>() {
      public int compare(JobTime jt1, JobTime jt2) {
         int val = jt1.time - jt2.time;
         return val != 0 ? val : jComp.compare(jt1.job, jt2.job);
      }
   };

   public JobTime[] makeSchedule(int[] wTimes, Job[] jobs) {
      int time, ndx, max;
      Job[] jArr = jobs.clone();
      TimeNodeSet tSet;
      TimeNode[] tnArr;
      JobTime[] jtArr = new JobTime[0];
      List<JobTime> jtList = new ArrayList<JobTime>(jobs.length);

      max = 0;
      for (ndx = 0; ndx < wTimes.length; ++ndx)
         if (wTimes[ndx] > max)
            max = wTimes[ndx];

      tnArr = new TimeNode[max];
      for (ndx = 0; ndx < wTimes.length; ++ndx) {
         if (wTimes[ndx] != 0) {
            if (tnArr[wTimes[ndx] - 1] == null)
               tnArr[wTimes[ndx] - 1] = new TimeNode(wTimes[ndx] - 1, 1);
            else
               ++tnArr[wTimes[ndx] - 1].quantity;
         }
      }

      for (ndx = tnArr.length - 2; ndx >= 0; --ndx) {
         if (tnArr[ndx] == null)
            tnArr[ndx] = new TimeNode(ndx, 0);
         tnArr[ndx].quantity += tnArr[ndx + 1].quantity;
      }

      Arrays.sort(jArr, jComp);

      tSet = new TimeNodeSet(tnArr);

      for (Job j : jArr) {
         time = tSet.getLatestTimeBeforeDeadline(j);
         if (time < 0) {
            time = tSet.getLatestTime();
            if (time < 0)
               break;
         }
         jtList.add(new JobTime(j, time));
      }

      jtArr = jtList.toArray(jtArr);
      Arrays.sort(jtArr, jtComp);

      return jtArr;
   }
}
